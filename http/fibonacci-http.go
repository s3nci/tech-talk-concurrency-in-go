package main

import (
	"concurrency-in-go/recursive"
	"fmt"
	"net/http"
	"strconv"
)
import "github.com/gin-gonic/gin"

func main() {
	router := gin.Default()

	router.GET("/fibonacci/:n", func(c *gin.Context) {
		nString := c.Param("n")
		if n, err := strconv.Atoi(nString); err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("%v is not an integer!", n))
		} else {
			response := fmt.Sprintf("The fibonacci number of %v is %v", n, recursive.Fibonacci(n))
			c.String(http.StatusOK, response)
		}
	})

	router.Run()
}
