package recursive

import (
	"fmt"
	"time"
)

var jobCounter = 0
var rangeStart = 30
var rangeEnd = 45

func main() {
	start := time.Now()
	for i := rangeStart; i < rangeEnd; i++ {
		jobCounter++
		printFibonacci(i)
	}
	durationIterative := time.Now().Sub(start)
	fmt.Printf("Execution time iterative: %v\n\n", durationIterative)

	start = time.Now()
	for i := rangeStart; i < rangeEnd; i++ {
		jobCounter++
		go printFibonacci(i)
	}
	for jobCounter > 0 {
		time.Sleep(1 * time.Millisecond)
	}
	durationConcurrent := time.Now().Sub(start)
	fmt.Printf("Execution time concurrent: %v\n\n", durationConcurrent)
	fmt.Printf("Execution time δ: %v\n", durationIterative - durationConcurrent)
}

func printFibonacci(n int) {
	fmt.Printf("The fibonacci number of %v is %v\n", n, Fibonacci(n))
	jobCounter--
}

// computational complexity: O(2^n)
func Fibonacci(n int) int {
	switch n {
	case 0:
		return 0
	case 1:
		return 1
	default:
		return Fibonacci(n-1) + Fibonacci(n-2)
	}
}
